/*
Navicat MySQL Data Transfer

Source Server         : 测试
Source Server Version : 80011
Source Host           : localhost:3306
Source Database       : mapdatabase

Target Server Type    : MYSQL
Target Server Version : 80011
File Encoding         : 65001

Date: 2018-12-13 11:31:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `area_data`
-- ----------------------------
DROP TABLE IF EXISTS `area_data`;
CREATE TABLE `area_data` (
  `id` bigint(20) NOT NULL,
  `data` varchar(6000) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_id` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of area_data
-- ----------------------------
INSERT INTO `area_data` VALUES ('134', '[{\"xPos\":108.993896,\"yPos\":34.391844},{\"xPos\":109.150273,\"yPos\":34.374684},{\"xPos\":109.062885,\"yPos\":34.308394}]', '2131', '98', '2018-12-08 15:58:23');
INSERT INTO `area_data` VALUES ('135', '[{\"xPos\":109.263531,\"yPos\":34.389938},{\"xPos\":109.242834,\"yPos\":34.353706},{\"xPos\":109.264106,\"yPos\":34.322229}]', '23121', '98', '2018-12-08 16:30:11');
INSERT INTO `area_data` VALUES ('136', '[{\"xPos\":108.745245,\"yPos\":34.368248},{\"xPos\":108.892998,\"yPos\":34.365865},{\"xPos\":108.803886,\"yPos\":34.290024}]', '213', '89', '2018-12-11 17:12:25');
INSERT INTO `area_data` VALUES ('137', '[{\"xPos\":108.914557,\"yPos\":34.352037},{\"xPos\":109.009993,\"yPos\":34.347745},{\"xPos\":108.93353,\"yPos\":34.297659}]', '13213', '98', '2018-12-11 17:32:10');
INSERT INTO `area_data` VALUES ('138', '[{\"xPos\":109.232198,\"yPos\":34.327714},{\"xPos\":109.404673,\"yPos\":34.406856},{\"xPos\":109.467913,\"yPos\":34.377783},{\"xPos\":109.396049,\"yPos\":34.300522}]', '12413', '98', '2018-12-11 17:36:53');

-- ----------------------------
-- Table structure for `company_data`
-- ----------------------------
DROP TABLE IF EXISTS `company_data`;
CREATE TABLE `company_data` (
  `id` bigint(20) NOT NULL,
  `company_domain_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `company_code` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `statue` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_r9a4kvronjxj02fbbi0rnfqn3` (`company_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_data
-- ----------------------------
INSERT INTO `company_data` VALUES ('98', 'www.915566.com', '智速信息技术有限公司', '123456', 'zhisu', 'zskj', '2018-09-04 17:58:05', '1');
INSERT INTO `company_data` VALUES ('101', 'www.libai.com', '李白科技', '123456', 'libai', 'lbkj', '2018-09-05 09:50:45', '0');

-- ----------------------------
-- Table structure for `hibernate_sequence`
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES ('139');

-- ----------------------------
-- Table structure for `manage_data`
-- ----------------------------
DROP TABLE IF EXISTS `manage_data`;
CREATE TABLE `manage_data` (
  `id` bigint(20) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `passwd` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of manage_data
-- ----------------------------
INSERT INTO `manage_data` VALUES ('78', '2018-08-31 14:57:07', '1234567', 'zhisu');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `userName` varchar(32) DEFAULT NULL COMMENT '用户名',
  `passWord` varchar(32) DEFAULT NULL COMMENT '密码',
  `user_sex` varchar(32) DEFAULT NULL,
  `nick_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
